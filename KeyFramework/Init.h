#ifndef _KEYFRAMEWORK_H_
#define _KEYFRAMEWORK_H_
#define _KEYFRAMEWORK_V_ "1.0.0"
#ifndef KEYBOARD_h
#include "Keyboard.h"
#endif
#ifndef MOUSE_h
#include "Mouse.h"
#endif

#ifndef KFR_KEYGEN_DISABLE
#ifndef KFR_KEYGEN_VERSION
#define KFR_KEYGEN_VERSION 1
#include "KeyGen/Init.h"
#endif
#endif

#include "Core.h"

#endif
