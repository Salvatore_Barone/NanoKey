#ifdef _KEYFRAMEWORK_H_
#ifndef _KEYFRAMEWORK_KEYGEN_1_H_
#define _KEYFRAMEWORK_KEYGEN_1_H_

#ifndef KFR_KEYGEN_SVAL_A
#define KFR_KEYGEN_SVAL_A 12
#endif
#ifndef KFR_KEYGEN_SVAL_B
#define KFR_KEYGEN_SVAL_B 7
#endif
#ifndef KFR_KEYGEN_SEQMOD
#define KFR_KEYGEN_SEQMOD 7
#endif
#ifndef KFR_KEYGEN_LENGTH
#define KFR_KEYGEN_LENGTH 12
#endif
#define KFR_KEYGEN_ALPHABET {"A", "B", "C", "D", "E", "F", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"}

String KFRKeyGenAlphabet[] = KFR_KEYGEN_ALPHABET;
int KFRKeyGenRandom;

void KFRKeyGen(bool newline) {
    for(int i = 0; i < KFR_KEYGEN_LENGTH; i++) {
        KFRKeyGenRandom = random(0, (sizeof(KFRKeyGenAlphabet) - 1));
        if(KFRKeyGenRandom <= ((sizeof(KFRKeyGenAlphabet) - 1) /2)) {
            KFRKeyGenRandom += KFR_KEYGEN_SVAL_A - KFR_KEYGEN_SEQMOD;
        } else {
            KFRKeyGenRandom -= KFR_KEYGEN_SVAL_B + KFR_KEYGEN_SEQMOD;
        }
        if(KFRKeyGenRandom > (sizeof(KFRKeyGenAlphabet) - 1)) {
            KFRKeyGenRandom = sizeof(KFRKeyGenAlphabet) - 1;
        } else if(KFRKeyGenRandom < 0) {
            KFRKeyGenRandom = 0;
        }
        Keyboard.print(KFRKeyGenAlphabet[KFRKeyGenRandom]);
    }
    if(newline) {
        Keyboard.println("");
    }
}

void KFRKeyGenNormal() {}

void KFRKeyGenAlt() {}

void KFRBegin(void *MainFunction = &KFRKeyGenNormal, void *AltFunction = &KFRKeyGenAlt);
#endif
#endif
