#ifdef  _KEYFRAMEWORK_H_
#ifndef _KEYFRAMEWORK_KEYGEN_0_H_
#define _KEYFRAMEWORK_KEYGEN_0_H_

#ifndef KFR_KEYGEN_SVAL_A
#define KFR_KEYGEN_SVAL_A 0
#endif
#ifndef KFR_KEYGEN_SVAL_B
#define KFR_KEYGEN_SVAL_B 1
#endif
#ifndef KFR_KEYGEN_SEQMOD
#define KFR_KEYGEN_SEQMOD 7
#endif
#ifndef KFR_KEYGEN_LENGTH
#define KFR_KEYGEN_LENGTH 7
#endif

void KFRKeyGen(bool newline) {
  unsigned char A = KFR_KEYGEN_SVAL_A;
  unsigned char B = KFR_KEYGEN_SVAL_B;
  unsigned char C = 0;
  unsigned char X = KFR_KEYGEN_SEQMOD;
  unsigned char Z = 0;
  unsigned char L = KFR_KEYGEN_LENGTH;
  for(unsigned char i = 0; i != L; i++) {
    C = A + B;
    A = B;
    B = C;
    Z = C;
    if(Z <= 64) {
      Z += X;
    } else if(Z <= 127) {
      Z -= X;
    } else if(Z <= 191) {
      Z *= X;
    } else {
      Z /= X;
    }
    String TMPZ = String(Z, HEX);
    if(TMPZ.length() < 2) {
      TMPZ = String("0" + TMPZ);
    }
    Keyboard.print(TMPZ);
  }
  if(newline) {
    Keyboard.println("");
  }
}

void KFRKeyGenNormal() {
  KFRKeyGen(true);
}

void KFRKeyGenAlt() {
  KFRKeyGen(false);
}

void KFRBegin(void *MainFunction = &KFRKeyGenNormal, void *AltFunction = &KFRKeyGenAlt);

#endif
#endif
