#ifdef  _KEYFRAMEWORK_H_
#ifndef _KEYFRAMEWORK_CORE_H
#define _KEYFRAMEWORK_CORE_H

#ifndef KFR_PIN_RESEND
#define KFR_PIN_RESEND 4
#endif
#ifndef KFR_PIN_ALTSEND
#define KFR_PIN_ALTSEND 5
#endif
#ifndef KFR_START_DELAY
#define KFR_START_DELAY 2000
#endif
#ifndef KFR_DEBUG
#define KFR_DEBUG false
#endif
#ifndef KFR_BAUDRATE
#define KFR_BAUDRATE 115200
#endif
#ifndef KFR_DELAY
#define KFR_DELAY 1000
#endif

void (*KFRMain)();
void (*KFRMainAlt)();
bool KFRDoLoop = true;
bool KFRExecAlt = false;

void KFRBegin(void *MainFunction, void *AltFunction) {
  Keyboard.begin();
  Mouse.begin();
  KFRMain = MainFunction;
  KFRMainAlt = AltFunction;
  #ifdef KFR_DEBUG
  #if KFR_DEBUG == true
  Serial.begin(KFR_BAUDRATE);
  Serial.print("KeyFramework V_"); Serial.println( _KEYFRAMEWORK_V_);
  Serial.println("Using:");
  Serial.print("  Baudrate: "); Serial.println(KFR_BAUDRATE);
  Serial.print("  Start delay: "); Serial.print(KFR_START_DELAY); Serial.println(" ms");
  Serial.print("  General delay: "); Serial.print(KFR_DELAY); Serial.println(" ms");
  Serial.print("  Re-Send pin: "); Serial.println(KFR_PIN_RESEND);
  #ifndef KFR_KEYGEN_DISABLE
  Serial.print("  KeyGen Version: "); Serial.println(KFR_KEYGEN_VERSION);
  #else
  Serial.println("  KeyGen Version: Disabled");
  #endif
  Serial.println("  Modules:");
  Serial.println("    Keyboard.h");
  Serial.println("    Mouse.h");
  #endif
  #endif
  delay(KFR_START_DELAY);
}

void KFRLoop() {
  if(KFRDoLoop) {
    #ifdef KFR_DEBUG
    #if KFR_DEBUG == true
    Serial.println("Started key function.");
    #endif
    #endif
    if(!KFRExecAlt) {
      (*KFRMain)();
    } else {
      (*KFRMainAlt)();
    }
    KFRDoLoop = false;
    #ifdef KFR_DEBUG
    #if KFR_DEBUG == true
    Serial.println("Stoped key function.");
    #endif
    #endif
  }
}

#endif
#endif
